FROM node:lts-alpine

# install simple http server for serving static content

WORKDIR /app
RUN npm install -g serve
# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./

RUN npm install
COPY . .
RUN npm run build



EXPOSE 8080
CMD ["serve","-s","build", "-p", "8080"]
