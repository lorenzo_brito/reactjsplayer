import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
           Music streaming from Azure Service Media - automated change test 
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <ol>
          <li><iframe src="//aka.ms/ampembed?url=%2F%2Ftestmediaservice-usct.streaming.media.azure.net%2Fd9eb372d-a5bd-4600-8575-ca1f21fb2e04%2FEstrenon%202.ism%2Fmanifest" name="azuremediaplayer" scrolling="no" frameborder="no" align="center" height="280px" width="500px" allowfullscreen></iframe></li>
          <li>
          <iframe src="//aka.ms/ampembed?url=%2F%2Ftestmediaservice-usct.streaming.media.azure.net%2Fff6b9ac6-55e1-4d32-ad0e-f7a8d998c436%2Ffile_example_MP3_5MG.ism%2Fmanifest" name="azuremediaplayer" scrolling="no" frameborder="no" align="center" height="280px" width="500px" allowfullscreen></iframe>
          </li>
        </ol>
      </header>
    </div>
  );
}

export default App;
